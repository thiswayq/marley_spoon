class ContentfulService
  class << self
    def recipes
      client = register_client(ENV['CONTENTFUL_RECIPES_ACCESS_TOKEN'], ENV['CONTENTFUL_RECIPES_SPACE'])
      recipes = RecipesFactory.new(client)
      recipes.build
    end
    
    private

    def register_client(access_token, space)
      Contentful::Client.new(
        access_token: access_token,
        space: space,
        dynamic_entries: :auto
      )
    end
  end
end