class RecipesFactory
  def initialize(client)
    @client = client 
  end

  def build
    recipes = []
    @client.entries.map do |recipe| 
      next unless recipe.content_type.sys[:id] == 'recipe'
      recipes << {
        title: get_title(recipe),
        image: get_image(recipe),
        details: {
          title: get_title(recipe),
          image: get_image(recipe),
          tags: get_tags(recipe),
          description: get_description(recipe),
          chef: get_chef(recipe)
        }
      }
    end
    recipes
  end

  private

  def get_chef(recipe)
    recipe.chef.name
  rescue
    return "No chef."
  end

  def get_tags(recipe)
    recipe.tags.map(&:name)
  rescue
    return ["No tags yet!"]
  end

  def get_title(recipe)
    recipe.title
  rescue
    return "We don't have a title, but click here, it's probably good."
  end

  def get_description(recipe)
    recipe.description
  rescue
    return "It's great!"
  end

  def get_image(recipe)
    recipe.photo.url
  rescue
    return "https://i.pinimg.com/originals/9a/81/97/9a81975c7262954391b4bf1c760b2e77.jpg"
  end
end