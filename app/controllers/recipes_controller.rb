class RecipesController < ApplicationController
  def index
    @recipes = ContentfulService.recipes
  end

  def show
    # Ignoring safe params here because we don't have a db for injections.
    @details = request.params[:details]
  end
end
