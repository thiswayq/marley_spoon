require 'rails_helper'
require "ostruct"

RSpec.describe RecipesFactory do
  let(:recipe) { OpenStruct.new({
    chef: OpenStruct.new({ name: "Some chef." }), 
    tags: [OpenStruct.new({ name: "Tag1" })], 
    title: "Some title", 
    description: "Some description.", 
    content_type: OpenStruct.new({sys: {id: 'recipe'}}),
    photo: OpenStruct.new({url: "Some url."})}
  )}

  let(:client) { OpenStruct.new({ entries: [recipe] })}

  describe "Creates a new recipe object" do 
    it "should create with the client params" do 
      factory = described_class.new(client).build
      expect(factory).to eq([{
        title: recipe.title,
        image: recipe.photo.url,
        details: {
          title: recipe.title,
          image: recipe.photo.url,
          tags: [recipe.tags.first.name],
          description: recipe.description,
          chef: recipe.chef.name
        }
      }])
    end
  end
end
