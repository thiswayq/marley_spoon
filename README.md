# Recipes project for Marley Spoon

## Overview
  Project was created focusing on the back-end, so that's why there's no styling on the html files.
  You can see it live at: https://marley-spoon-maria.herokuapp.com/recipes/index.
  

## Installation:
  - Clone repository
  - Bundle install
  - Create .env file with the 'CONTENTFUL_RECIPES_ACCESS_TOKEN' and 'CONTENTFUL_RECIPES_SPACE'
  - rails start to initialize.
 
## Tests
  - Could've tested more classes but I thought I already spent some time doing everything, so decided to just test the critical one.

